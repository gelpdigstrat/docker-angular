# docker-angular-cli

For creating/building angular projects. Bundles node + yarn + angular cli

## How to use this image

```
docker run -v /my/app:/src telusgelp/angular-cli ash -c 'yarn install && ng build --prod'
```
